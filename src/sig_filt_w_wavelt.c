/**
 * DOXYGEN COMMENTS
 *
 * @file   sig_filt_w_wavelt.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   17/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

// Add include's here

#include "sig_filt_w_wavelet.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

#define     ERROR_EXIT(x,y)         perror(x);exit(y)

#define     ERROR_CHECK(x)          if(NULL==x){fprintf(stderr,"Error variable %s",#x);exit(-1);}

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

void sig_filt_w_wavelet(double *buffer, int buffSize, const gsl_wavelet_type *motherWaveFamily, int motherWaveFamilyLevel)
{
    int i=0;
    gsl_wavelet *w = NULL;
    gsl_wavelet_workspace *work = NULL;

    //Se crea una "wavelet madre"
    w = gsl_wavelet_alloc (motherWaveFamily, motherWaveFamilyLevel);
    ERROR_CHECK(w);

    work = gsl_wavelet_workspace_alloc (buffSize);
    ERROR_CHECK(work);

    //Se aplica la transformada wavelet, de niveles impuestos por log2(N), en este caso N=64 => 6 niveles
    if(gsl_wavelet_transform_forward (w, buffer, 1, buffSize, work) != GSL_SUCCESS)
    { ERROR_EXIT("gsl_wavelet_transform_forward error:",14); } //Se controlan posibles errores

    //Se eliminan todos los niveles que no correspondan al nivel "3" (el 4o según el plot de Matlab)
    for(i=0;i<buffSize;i++)    if(i<4 || i>7)  buffer[i]=0;

    //Se aplica la transformada inversa de wavelet para recomponer la señal comprimida
    if(gsl_wavelet_transform_inverse (w, buffer, 1, buffSize, work) != GSL_SUCCESS)
    { ERROR_EXIT("gsl_wavelet_transform_inverse error:",15); } //Se controlan posibles errores

    //Se liberan los recursos
    gsl_wavelet_free (w);
    gsl_wavelet_workspace_free (work);
}
