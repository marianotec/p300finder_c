/**
 * DOXYGEN COMMENTS
 *
 * @file   get_energy_from_buffer.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   17/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

// Add include's here

#include "math.h"

//#define ENERGY_DEBUG

#ifdef  ENERGY_DEBUG
#include "stdio.h"
#endif
/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

#define     SQUARE(x)               (x*x)

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

long double get_energy_from_buffer(double * buffer, int bufferSize)
{
    int i = 0;
    long double energy = 0.0;

    for(i = 0; i < bufferSize; i++)
        energy += (long double) SQUARE(fabs(buffer[i]));

#ifdef  ENERGY_DEBUG
    printf("\nEnergy: %Lf\n",energy);
#endif

    return energy;
}
