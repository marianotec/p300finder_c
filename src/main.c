/**
 * DOXYGEN COMMENTS
 *
 * @file   main.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   15/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

#include "main.h"

#include "sig_filt_w_wavelet.h"

#include "math.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

//#define DEBUG
//#define DEBUG_PLOT_PROCESSED_SIGNAL
#define ENERGY_COMPARE

//#define WITH_AC_SUBSTRACTION
#ifndef WITH_AC_SUBSTRACTION
//#define WITH_NORMALIZATION
#endif

#ifdef  WITH_AC_SUBSTRACTION
//#define DEBUG_PLOT_WO_AC_SIGNAL
#endif

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

#define     ELEC_INDEX_START(x)             eleccionIndex[ELECCION_START][x]
#define     ELEC_INDEX_END(x)               eleccionIndex[ELECCION_END][x]

#define     MARK_INDEX_SI(x)                markIndex[ELECCION_SI][x]
#define     MARK_INDEX_NO(x)                markIndex[ELECCION_NO][x]

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

int main (int argc, char * argv[])
{
    /* ********************************************************************************
     *                   DECLARACIÓN DE VARIABLES
     */

    int	auxChannel = 0, auxMarker = 0;

    long retVal = 0;

    int i = 0, j = 0;

#ifdef DEBUG
    int debugVar = 0;
    char debugCommand[100];
    for(i=0;i<100;i++)debugCommand[i]='\0';
#endif

    int indexOffset = 0;

    int *channelValues = NULL;
    char *channelMarkers = NULL;
    int channelLenghtSize = 0;

    double *avgSiValues = NULL;
    double *avgNoValues = NULL;

#ifdef WITH_NORMALIZATION
    double meanValue = 0, stdValue = 0, maxValue = 0;
#endif

    long double energySi = 0, energyNo = 0;

    int *eleccionIndex[2]={NULL,NULL};
    int startQtty = 0, endQtty = 0, eleccionSize = 0;

    int *markIndex[2]={NULL,NULL};
    int siQtty = 0, noQtty = 0;

    void * ptrAux = NULL;

    FILE* dataFile = NULL;

    /* ******************************************************************************************* */

    CLEAR_SCREEN;

    /*	****************************************************************************************
     *
     *				Se extraen los datos de interés de los archivos CVS
     *
     */

    if(argc < 2){ ERROR_EXIT("Error al ejecutar la aplicación.\nModo de uso: ./exec data_path\n",-2); }

    extract_columns_from_file(argv[1]);

    //Se abre el archivo reducido
    ERROR_CHECK(( dataFile = fopen(TEMP_DATA_FILENAME,READ_ONLY) ));	//Se abre el archivo csv con los datos

    do
    {
        //Extraigo los valores de las dos columnas del archivo y las almaceno en variables auxiliares
        retVal = fscanf(dataFile,COLUMN_FORMAT,&auxChannel,&auxMarker);

        if(EOF != retVal) //Solo se ingresa si se leyó algo del archivo
        {
            //Se pide memoria para realojar los buffers (tamaño variable)
            ERROR_CHECK(( channelValues = realloc(channelValues, INT * (channelLenghtSize+1)) ));
            ERROR_CHECK(( channelMarkers = realloc(channelMarkers, CHAR * (channelLenghtSize+1)) ));

            //Se asignan los valores a almacenar
            channelValues[channelLenghtSize] = auxChannel;
            channelMarkers[channelLenghtSize] = (char) auxMarker;

            MOVE_TO_NEXT_LINE(dataFile); //Muevo el "cursor" a la siguiente linea del archivo

            //Se guarda la información de interés en los distintos buffers según el Marker correspondiente
            switch(channelMarkers[channelLenghtSize])
            {

            case INICIO_ELECCION:
                eleccionIndex[ELECCION_START] = realloc(eleccionIndex[ELECCION_START], INT * (startQtty+1));
                ERROR_CHECK(eleccionIndex[ELECCION_START]);
                ELEC_INDEX_START(startQtty) = channelLenghtSize; //Se asignan los valores a almacenar
                startQtty++; //Se incrementa el indice del marker
                break;

            case FIN_ELECCION:
                eleccionIndex[ELECCION_END] = realloc(eleccionIndex[ELECCION_END], INT * (endQtty+1));
                ERROR_CHECK(eleccionIndex[ELECCION_END]);
                ELEC_INDEX_END(endQtty) = channelLenghtSize; //Se asignan los valores a almacenar
                endQtty++; //Se incrementa el indice del marker
                break;

            case MARK_SI:
                markIndex[ELECCION_SI] = realloc(markIndex[ELECCION_SI], INT * (siQtty+1));
                ERROR_CHECK(markIndex[ELECCION_SI]);
                MARK_INDEX_SI(siQtty) = channelLenghtSize; //Se asignan los valores a almacenar
                siQtty++; //Se incrementa el indice del marker
                break;

            case MARK_NO:
                markIndex[ELECCION_NO] = realloc(markIndex[ELECCION_NO], INT * (noQtty+1));
                ERROR_CHECK(markIndex[ELECCION_NO]);
                MARK_INDEX_NO(noQtty) = channelLenghtSize; //Se asignan los valores a almacenar
                noQtty++; //Se incrementa el indice del marker
                break;

            default: break;

            }

            channelLenghtSize++;
        }

    }while(EOF != retVal); //El loop termina cuando se llega al final del archivo

#ifdef DEBUG
    printf("startQtty: %d\n",startQtty);
    printf("endQtty: %d\n",endQtty);
    printf("siQtty %d\n",siQtty);
    printf("noQtty: %d\n",noQtty);
    printf("\n");
#endif

    fclose(dataFile);   //Se cierra el archivo ya que no se utiliza más
    free(channelMarkers); //Se libera la memoria del vector de Markers ya que se extrajeron los índices

    /* ******************************************************************************************* */

    /*	****************************************************************************************
     *
     *		Se recorta la porción de valores del canal que se encuentran dentro de la selección
     *
     */

    //Se obtiene el tamaño de la nueva selección según los distintos markers de elección
    if(NULL == eleccionIndex[ELECCION_END] && NULL != eleccionIndex[ELECCION_START])
    {
        indexOffset = ELEC_INDEX_START(0);
        eleccionSize = channelLenghtSize - indexOffset + 1;
    }

    else if(NULL == eleccionIndex[ELECCION_START] && NULL != eleccionIndex[ELECCION_END])
    {
        indexOffset = 0;
        eleccionSize = ELEC_INDEX_END(0) + 1;
    }

    else if(NULL == eleccionIndex[ELECCION_START] && NULL == eleccionIndex[ELECCION_END])
    {
        indexOffset = 0;
        eleccionSize = channelLenghtSize;
    }

    else
    {
        indexOffset = ELEC_INDEX_START(0);
        eleccionSize = channelLenghtSize - indexOffset;
    }

#ifdef DEBUG
    printf("indexOffset: %d\n",indexOffset);
    printf("eleccionSize: %d\n",eleccionSize);
    printf("\n");
#endif

    //A partir de este punto se hace una suerte de "swap" con memoria dinámica
    ERROR_CHECK(( ptrAux = malloc (INT * eleccionSize) ));

    for(i = 0; i < eleccionSize; i++)	((int*) ptrAux)[i] = channelValues[indexOffset+i];

    ERROR_CHECK(( channelValues = (int *) realloc(channelValues,INT * eleccionSize) ));

    for(i = 0; i < eleccionSize; i++)	channelValues[i] = ((int*) ptrAux)[i];

    //Se reajustan los índices para el nuevo buffer de datos
    for(i = 0; i < siQtty; i++) MARK_INDEX_SI(i) -= indexOffset;
    for(i = 0; i < noQtty; i++) MARK_INDEX_NO(i) -= indexOffset;

    //Se libera memoria que ya no se utiliza
    free(ptrAux);
    free(eleccionIndex[ELECCION_START]);
    free(eleccionIndex[ELECCION_END]);


    /* ******************************************************************************************* */

    /* *******************************************************************************************
     *
     *       Se promedian los segmentos de "si" y "no" en un único segmento para cada elección
     */

    //Se asigna memoria dinámica a los vectores
    ERROR_CHECK(( ptrAux = malloc (INT * AVG_BUFF_SIZE) ));

    ERROR_CHECK(( avgNoValues = (double *) malloc (DOUBLE * AVG_BUFF_SIZE) ));

    ERROR_CHECK(( avgSiValues = (double *) malloc (DOUBLE * AVG_BUFF_SIZE) ));

    /**
     * todo Solucionar bugs por mal indexación de archivos/archivos incompletos
     */
    // Tuve que hacer esto porque hay algunos archivos mal seccionados!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    noQtty--; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    siQtty--; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    for(i = 0; i < AVG_BUFF_SIZE; i++) ((int*) ptrAux)[i] = 0; //Se inicializa el buffer en 0

    //Se suman los "si"
    for(i = 0; (i < siQtty) && (0 <= MARK_INDEX_SI(i)) && (MARK_INDEX_SI(i) < eleccionSize); i++)
    {
        for(j = 0; j < AVG_BUFF_SIZE; j++)
        {
            ((int*) ptrAux)[j] += channelValues[MARK_INDEX_SI(i)+j];

        }
#ifdef DEBUG
        debugVar++;
#endif
    }

#ifdef DEBUG
    printf("times averaged: %d\n",debugVar);
    printf("\n");
#endif

    //Se promedian los "si"
    for(j = 0; j < AVG_BUFF_SIZE; j++)  avgSiValues[j] = ((double)((int*) ptrAux)[j]/(double)siQtty);

    for(i = 0; i < AVG_BUFF_SIZE; i++) ((int*) ptrAux)[i] = 0; //Se inicializa el buffer en 0

    //Se suman los "no"
    for(i = 0; i < noQtty; i++)
    {
        for(j = 0; j < AVG_BUFF_SIZE; j++)
        {
            if((MARK_INDEX_SI(i)+j < eleccionSize) && (0 <= MARK_INDEX_SI(i)+j))
                ((int*) ptrAux)[j] += channelValues[MARK_INDEX_NO(i)+j];
        }
    }

    //Se promedian los "no"
    for(j = 0; j < AVG_BUFF_SIZE; j++)  avgNoValues[j] = ((double)((int*) ptrAux)[j]/(double)noQtty);

    free(ptrAux);

    /* ******************************************************************************************* */

    /* *******************************************************************************************
     *
     *         Se normalizan los buffers de las selecciones "si" y "no" promediadas
     *
     *         función de normalización: Zi = (Xi-mean(X)) / std(X)
     */

#ifdef  WITH_AC_SUBSTRACTION

#ifndef WITH_NORMALIZATION

    //Se obtiene el valor medio del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) debugVar += (avgSiValues[i]);

    debugVar/=AVG_BUFF_SIZE;

    //Se obtiene el valor medio del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) avgSiValues[i]-=debugVar;

    debugVar = 0;

    for(i = 0; i < AVG_BUFF_SIZE; i++) debugVar += (avgNoValues[i]);

    debugVar/=AVG_BUFF_SIZE;

    //Se obtiene el valor medio del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) avgNoValues[i]-=debugVar;

#ifdef DEBUG_PLOT_WO_AC_SIGNAL

    printf("Saving SI/NO Without AC...\n");

    for(i=0;i<AVG_BUFF_SIZE;i++)
    {
        if(!i)  sprintf(debugCommand,"echo \"%lf, %lf\" > %s",avgSiValues[i],avgNoValues[i],"debug_woAC_file.csv");
        else    sprintf(debugCommand,"echo \"%lf, %lf\" >> %s",avgSiValues[i],avgNoValues[i],"debug_woAC_file.csv");
        system(debugCommand);
    }

    printf("\n");

#endif

#endif

#endif

#ifdef WITH_NORMALIZATION
    //Se obtiene el valor medio del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) meanValue += (avgSiValues[i]/AVG_BUFF_SIZE);

    //Se obtiene el desvio standard del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) stdValue += SQUARE((avgSiValues[i]-meanValue));
    stdValue = sqrt(stdValue/(AVG_BUFF_SIZE-1));

    //Se normaliza muestra a muestra
    for(i = 0; i < AVG_BUFF_SIZE; i++) avgSiValues[i] = (avgSiValues[i]-meanValue)/stdValue;

    //Se repite el proceso para el otro set de valores
    meanValue = stdValue = 0;

    //Se obtiene el valor medio del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) meanValue += (avgNoValues[i]/AVG_BUFF_SIZE);

    //Se obtiene el desvio standard del buffer
    for(i = 0; i < AVG_BUFF_SIZE; i++) stdValue += SQUARE((avgNoValues[i]-meanValue));
    stdValue = sqrt(stdValue/(AVG_BUFF_SIZE-1));

    //Se normaliza muestra a muestra
    for(i = 0; i < AVG_BUFF_SIZE; i++) avgNoValues[i] = (avgNoValues[i]-meanValue)/stdValue;
#endif

    /* ******************************************************************************************* */

    /* *********************************************************************************************
     *
     *	 Se pasa las señales por la transformada wavelet y se le suprimen los niveles no desados,
     *	 luego se recompone la señal.
     */

    /*
     * Se filtran las señales utilizando wavelets
     *
     * Wavelets Madre:
     * gsl_wavelet_daubechies -> lvls=4, 6, …, 20
     * gsl_wavelet_haar ->  lvls=2
     * gsl_wavelet_bspline -> lvls=103, 105, 202, 204, 206, 208, 301, 303, 305 307, 309
     *
     */
    sig_filt_w_wavelet(avgNoValues, AVG_BUFF_SIZE, BEST_WAVELET);
    sig_filt_w_wavelet(avgSiValues, AVG_BUFF_SIZE, BEST_WAVELET);

#ifdef DEBUG_PLOT_PROCESSED_SIGNAL
    printf("Saving SI/NO Processed...\n");

    for(i=0;i<AVG_BUFF_SIZE;i++)
    {
        if(i==0)  sprintf(debugCommand,"echo \"%lf, %lf\" > %s",avgSiValues[i],avgNoValues[i],"debug_proc_file.csv");
        else    sprintf(debugCommand,"echo \"%lf, %lf\" >> %s",avgSiValues[i],avgNoValues[i],"debug_proc_file.csv");
        system(debugCommand);
    }

    printf("\n");
#endif
    /* ******************************************************************************************* */

    /* ********************************************************************************************
     *
     *              Se comparan las energias de los vectores
     *
     */

    energySi = get_energy_from_buffer(avgSiValues, AVG_BUFF_SIZE);
    energyNo = get_energy_from_buffer(avgNoValues, AVG_BUFF_SIZE);

#ifdef ENERGY_COMPARE

    printf("Elección estimada: ");
    if(energySi>energyNo)   printf("*SI*\t");
    else                    printf("*NO*\t");

    printf("(%.2Lf%c)\n\n",((energySi-energyNo)/fabs(MAX_VALUE(energySi,energyNo)))*100,'%');
#endif


    /* ******************************************************************************************* */

    /* ********************************************************************************************
     *
     *              Se liberan los recursos del sistema
     *
     */
    //system("rm ./*.csv");
    free(avgSiValues);
    free(avgNoValues);
    free(markIndex[ELECCION_NO]);
    free(markIndex[ELECCION_SI]);
    free(channelValues);

    return 0;
}
