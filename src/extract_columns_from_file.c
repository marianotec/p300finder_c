/**
 * DOXYGEN COMMENTS
 *
 * @file   extract_columns_from_file.c
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   17/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/*                              INCLUDEs                            */

// Add include's here

#include "main.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// Add private define's here

/* **************************************************************** */
/*                              MACROs                              */

// Add private macro's here

/* **************************************************************** */
/*                              GLOBALs                             */

// Add global variable's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

// Add structure's, enum's, typedefs, etc, here

/* **************************************************************** */
/*                               CODE                               */

void extract_columns_from_file(char *filePath)
{
    char systemCommand[SYS_COMMAND_LENGHT];

    //Genero un archivo reducido respecto del csv original (de entrada)
    sprintf(systemCommand,"sed '1d' \"%s\" > tmp.dat",filePath); //Se saltea el renglon de cabecera
    system(systemCommand);

    //Se extraen las columnas de interes (colocar tantos %d,%d,... como sea necesario)
    sprintf(systemCommand,"cut -d',' -f%d,%d tmp.dat > %s",COL_CHO1,COL_MARKER,TEMP_DATA_FILENAME);
    system(systemCommand);

    system("rm ./tmp.dat");

    return;
}
