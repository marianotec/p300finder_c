mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

CC := gcc
 
SRC_PATH := ./src
INC_PATH := ./inc
OUT_PATH := ./bin
OBJ_PATH := ./obj

INCLUDES := -I$(INC_PATH)
LFLAGS   := -Wall
CFLAGS   := -Wall -c

SRC_FILES := $(wildcard $(SRC_PATH)/*.c)
INC_FILES := $(wildcard $(INC_PATH)/*.h)
OBJ_FILES := $(subst $(SRC_PATH),$(OBJ_PATH),$(SRC_FILES:.c=.o))

LIBS := -lm -lgsl -lgslcblas
LD_FILE := 
LIB_PATH := -L./lib

all: clear_screen directories $(PROJECT)

clear_screen:
	@clear

directories:
	@mkdir -p $(OBJ_PATH) $(OUT_PATH)
	
$(PROJECT): $(OBJ_FILES)
	@echo "*** Linking project $(PROJECT) ***"
	$(CC) $(LIB_PATH) $(LFLAGS) $(LD_FILE) -o $(OUT_PATH)/$(PROJECT).bin $(OBJ_FILES) $(LIBS)
	@echo ""

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@echo "*** Compiling C file $< ***"
	$(CC) $(INCLUDES) $(CFLAGS) $< -o $@ 
	@echo ""
	
clean:
	@clear
	@echo "*** Cleaning compilation directories ***"
	rm -r -f $(OBJ_PATH) $(OUT_PATH)
	@echo ""
