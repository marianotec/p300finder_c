/**
 * DOXYGEN COMMENTS
 *
 * @file   main.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   15/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

#ifndef MAIN_H_
#define MAIN_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

//	Add include's here

#include "stdio.h"

#include "string.h"

#include "stdlib.h"

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

#define         CANT_MUESTRAS           64

#define         AVG_BUFF_SIZE           CANT_MUESTRAS

#define         COL_CHO1                9

#define         COL_MARKER              36

#define         SYS_COMMAND_LENGHT      100

#define			DATA_PATH               "../estudios/outfile2.csv"

#define         TEMP_DATA_FILENAME      "temp_out.csv"

#define			WAVELET_TYPE            "coif4"

#define			COLUMN_FORMAT           "%d.%*d, %d.%*d\n"	//ex: 543.00000, 0.00000(\n)
//#define			COLUMN_FORMAT			"%d.00000000000000000000, %d.00000000000000000000\n"	//ex: 543.00000000000000000000, 0.00000000000000000000(\n)

// GENERIC DEFINEs

#define         ELECCION_START          0
#define         ELECCION_END            1

#define         ELECCION_SI             0
#define         ELECCION_NO             1

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define         fs                      128

#define         READ_ONLY               "r"

#define         BEST_WAVELET            gsl_wavelet_bspline,206     //Por ahora la "b spline 206" es la que mejor responde

// OTHER DEFINEs

#define         INICIO_ELECCION         '@'		//caracter que indica el comienzo del experimento

#define         FIN_ELECCION            '$'		//caracter que indica el fin del experimento

#define         MARK_SI                 '%'		//caracter que indica el comienzo del estímulo 1

#define         MARK_NO                 '#'		//caracter que indica el comienzo del estímulo 2

#define         REALLOC_ERROR           "Error con realloc: "



/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

//	Add macro's here

#define     CLEAR_SCREEN            system("clear")

#define     ERROR_EXIT(x,y)         perror(x);exit(y)

#define     SQUARE(x)               (x*x)

#define     INT                     sizeof(int)

#define     CHAR                    sizeof(char)

#define     DOUBLE                  sizeof(double)

#define     MAX_VALUE(x,y)          (energySi>energyNo?energySi:energyNo)

#define     MOVE_TO_NEXT_LINE(dataFile)     while(fgetc(dataFile)!='\n') //Tener cuidado, no verifica EOF

#define     ERROR_CHECK(x)                  if(NULL==x){fprintf(stderr,"Error variable %s",#x);exit(-1);}

/* **************************************************************** */
/*                             DATA TYPEs                           */

//	Add data type's here

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

//	Add global variable's here

/* **************************************************************** */
/*                              PROTOTYPEs                          */

//	Add function prototype's here

void extract_columns_from_file(char *filePath);

long double get_energy_from_buffer(double * buffer, int bufferSize);

#endif /* MAIN_H_ */
