/**
 * DOXYGEN COMMENTS
 *
 * @file   sig_filt_w_wavelet.h
 *
 * @Author Koremblum, Nicolás Mariano (nkoremblum@frba.utn.edu.ar)
 *
 * @date   17/10/2016 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

#ifndef SIG_FILT_W_WAVELET_H_
#define SIG_FILT_W_WAVELET_H_

/* **************************************************************** */
/*                              INCLUDEs                            */

//	Add include's here

#include <gsl/gsl_sort.h>

#include <gsl/gsl_wavelet.h>

/* **************************************************************** */
/*                              DEFINEs                             */

// CONFIGURATION DEFINEs

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

// OTHER DEFINEs

/* **************************************************************** */
/*                              IFDEFs                              */

//	Add ifdef's here

/* **************************************************************** */
/*                              MACROs                              */

//	Add macro's here

/* **************************************************************** */
/*                             DATA TYPEs                           */

//	Add data type's here

/* **************************************************************** */
/*                           EXTERNAL GLOBALs                       */

//	Add global variable's here

/* **************************************************************** */
/*                              PROTOTYPEs                          */

//	Add function prototype's here

void sig_filt_w_wavelet(double *buffer, int buffSize, const gsl_wavelet_type *motherWaveFamily, int motherWaveFamilyLevel);

#endif /* SIG_FILT_W_WAVELET_H_ */
